package com.controllers.admin.implementation;

import com.controllers.admin.IAdminFrameController;
import com.controllers.admin.IAdminProfileController;
import com.model.Admin;
import com.views.admin.AdminFrame;

public class AdminFrameController implements IAdminFrameController {

	private IAdminProfileController adminProfileController;
	private AdminFrame adminFrame;

	public AdminFrameController(IAdminProfileController adminProfileController) {
		this.adminProfileController = adminProfileController;
	}

	@Override
	public void setAdmin(Admin admin) {
		adminProfileController.setAdmin(admin);
	}

	
	@Override
	public void setAdminFrame(AdminFrame adminFrame) {
		this.adminFrame = adminFrame;
	}

	@Override
	public void openFrame() {
		adminFrame.setVisible(true);
	}

}
