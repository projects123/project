package com.controllers.admin.implementation;

import com.controllers.admin.IAdminModifyUserController;
import com.model.Specialization;
import com.model.User;
import com.model.dao.AdminDAO;
import com.model.dao.SpecializationDAO;
import com.model.dao.StudentDAO;
import com.model.dao.TeacherDAO;
import com.views.admin.ModifyUser;

public class AdminModifyUsersController implements IAdminModifyUserController {

	private User selectedUser;
	
	private ModifyUser adminModifyUsers;

	private AdminDAO adminDAO;

	private TeacherDAO teacherDAO;

	private StudentDAO studentDAO;
	
	private final int SELECTED_ADMIN = 0;
	private final int SELECTED_TEACHER = 1;
	private final int SELECTED_STUDENT = 2;

	private int selected;
	private String role;

	private SpecializationDAO specializationDAO;
	

	@Override
	public void selectSpecialization(Specialization specialization) {
		// TODO Auto-generated method stub
		
	}
	
	public void openFrame() {
		adminModifyUsers.ereaseAll();
		
		if(SELECTED_STUDENT == selected) {
			adminModifyUsers.populateSpecializations(specializationDAO.getAllSpecializations());
		}

		itemsForSelectedUser();
		adminModifyUsers.setVisible(true);
		
		
	}
	@Override
	public void saveUser() {
		
			 
			//adminDAO.(selectedUser.);
		
			 
		   //  teacherDAO.updateUser(selectedUser);
	
			 
			//studentDAO.getStudent(selectedUser.getCnp());
		 
		
	}
	
	public void saveU() {
		
		selectedUser.setName(adminModifyUsers.getName());
		selectedUser.setSurname(adminModifyUsers.getSurname());
		selectedUser.setCnp(adminModifyUsers.getCNP());
		//selectedUser.setRole(adminModifyUsers.getRole());
	}

	@Override
	public void selectAdmin() {
		selected = SELECTED_ADMIN;
		role = "admin";
		
	}

	@Override
	public void selectStudent() {
		selected = SELECTED_STUDENT;
		role = "student";
	}

	@Override
	public void selectTeacher() {
		selected = SELECTED_TEACHER;
		role ="teacher";
		
	}
	
	private void itemsForSelectedUser() {
		adminModifyUsers.setName(selectedUser.getName());
		adminModifyUsers.setSurname(selectedUser.getSurname());
		adminModifyUsers.setCnp(selectedUser.getCnp());
		adminModifyUsers.setRole(selectedUser.getRole());
		
	}

}
