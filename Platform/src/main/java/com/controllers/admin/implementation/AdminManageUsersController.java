package com.controllers.admin.implementation;

import java.util.List;

import com.controllers.admin.IAdminCreateUserController;
import com.controllers.admin.IAdminManageUsersController;
import com.model.User;
import com.model.dao.AdminDAO;
import com.model.dao.StudentDAO;
import com.model.dao.TeacherDAO;
import com.model.dao.UserDAO;
import com.views.admin.AdminPanel;
import com.views.admin.ManageUsers;

public class AdminManageUsersController  implements IAdminManageUsersController{

	private IAdminCreateUserController adminCreateUserController;
	
	private UserDAO usersDAO;
	
	private AdminDAO adminDAO;
	
	private TeacherDAO teacherDAO;
	
	private StudentDAO studentDAO;

	private ManageUsers adminManageUsers;
	
	List<User> usersList;

	private User selectedUser;

	private AdminPanel adminPanel;
	
	

	public AdminManageUsersController(UserDAO usersDAO, AdminDAO adminDAO, TeacherDAO teacherDAO, StudentDAO studentDAO,
			IAdminCreateUserController adminCreateUserController) {
		super();
		this.usersDAO = usersDAO;
		this.adminDAO = adminDAO;
		this.teacherDAO = teacherDAO;
		this.studentDAO = studentDAO;
		this.adminCreateUserController = adminCreateUserController;
	}
	
	
	@Override
	public void setAdminManageUsers(ManageUsers adminManageUsers) {
		this.adminManageUsers = adminManageUsers;
	}


	@Override
	public void setAdminPanel(AdminPanel adminPanel) {
		this.adminPanel = adminPanel;
	}



	@Override
	public void openCreateUserFrame() {
		adminCreateUserController.openFrame();
		
	}

	@Override
	public void openModifyUserFrame(Object selectedUser) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteUser(User selectedUser) {
	
		usersDAO.deleteUser(selectedUser);
		
		if(selectedUser.getRole().equals("admin")) {
			adminDAO.deleteUser(selectedUser);
		}
		else if (selectedUser.getRole().equals("teacher")) {
			teacherDAO.deleteUser(selectedUser);
		}
		else {
			studentDAO.deleteUser(selectedUser);
		}
		
		
	}

	@Override
	public void userSelected(User user) {
		selectedUser = user;
	}
	
	public void viewUsers() {
		stateChanged();
		adminPanel.setPanel(adminManageUsers);
	}

	private void stateChanged() {
		usersList = usersDAO.getAllUsers();
		adminManageUsers.populate(usersList);
		
	}

	@Override
	public void search() {
		// TODO Auto-generated method stub
		
	}

}
