package com.controllers.student.implementation;

import com.controllers.student.IStudentEnrolledCoursesPanelController;
import com.controllers.student.IStudentFindCoursePannelController;
import com.controllers.student.IStudentFrameController;
import com.controllers.student.IStudentProfileInformationPanelController;
import com.model.Student;
import com.views.student.StudentFrame;

public class StudentFrameController implements IStudentFrameController {

	private IStudentProfileInformationPanelController studentProfilePanelController;
	private IStudentFindCoursePannelController studentFindCoursePannelController;
	private IStudentEnrolledCoursesPanelController studentEnrolledCoursesPanelController;
	private StudentFrame studentFrame;

	public StudentFrameController(IStudentProfileInformationPanelController studentProfilePanelController,
			IStudentFindCoursePannelController studentFindCoursePannelController,
			IStudentEnrolledCoursesPanelController studentEnrolledCoursesPanelController) {
				this.studentProfilePanelController = studentProfilePanelController;
				this.studentFindCoursePannelController = studentFindCoursePannelController;
				this.studentEnrolledCoursesPanelController = studentEnrolledCoursesPanelController;
	}
	
	
	@Override
	public void setStudentFrame(StudentFrame studentFrame) {
		this.studentFrame = studentFrame;
	}



	@Override
	public void openFrame() {
		studentFrame.setVisible(true);
	}

	@Override
	public void setStudent(Student student) {
		studentProfilePanelController.setStudent(student);
		studentFindCoursePannelController.setStudent(student);
		studentEnrolledCoursesPanelController.setStudent(student);
	}

}
