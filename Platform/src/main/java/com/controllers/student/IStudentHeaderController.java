package com.controllers.student;

import com.views.student.StudentPanel;

public interface IStudentHeaderController {

	void viewProfile();

	void viewCourses();
	
	void viewEmail();

}
