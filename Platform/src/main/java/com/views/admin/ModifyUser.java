package com.views.admin;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import com.controllers.admin.IAdminModifyUserController;
import com.model.Specialization;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Font;
import com.views.common.ILanguagePanel;

public class ModifyUser extends JPanel implements ILanguagePanel {

	private JTextField nameTextField;
	private JTextField surnameTextField;
	private JTextField cnpTextField;
	private JTextField internalEmailTextField;
	private JTextField externalEmailTextField;
	private JTextField passwordTextField;

	private JLabel lblName;
	private JLabel lblSurname;
	private JLabel lblCnp;
	private JLabel lblSpecialization;
	private JLabel lblInternalEmail;
	private JLabel lblExternalEmail;
	private JLabel lblPassword;

	private JButton btnSave;

	private JRadioButton rdbtnAdmin;
	private JRadioButton rdbtnStudent;
	private JRadioButton rdbtnTeacher;

	private JComboBox<Specialization> specializationComboBox;

	protected IAdminModifyUserController adminModifyUserController;
	private JTextField studyYearTextField;
	private JLabel lblStudyYear;

	public ModifyUser(IAdminModifyUserController adminModifyUserController) {
		setBounds(new Rectangle(0, 0, 209, 31));
		
		this.adminModifyUserController = adminModifyUserController;
		initialize();
	}

	public void initialize() {
		setBackground(new Color(215, 228, 242));
		setLayout(null);

		nameTextField = new JTextField();
		nameTextField.setSize(new Dimension(354, 31));
		nameTextField.setBounds(201, 33, 354, 31);
		add(nameTextField);
		nameTextField.setColumns(10);

		surnameTextField = new JTextField();
		surnameTextField.setSize(new Dimension(354, 31));
		surnameTextField.setBounds(201, 76, 354, 31);
		add(surnameTextField);
		surnameTextField.setColumns(10);

		cnpTextField = new JTextField();
		cnpTextField.setBounds(201, 130, 354, 31);
		add(cnpTextField);
		cnpTextField.setColumns(10);

		internalEmailTextField = new JTextField();
		internalEmailTextField.setColumns(10);
		internalEmailTextField.setBounds(201, 173, 354, 31);
		add(internalEmailTextField);

		externalEmailTextField = new JTextField();
		externalEmailTextField.setColumns(10);
		externalEmailTextField.setBounds(201, 212, 354, 31);
		add(externalEmailTextField);

		passwordTextField = new JTextField();
		passwordTextField.setColumns(10);
		passwordTextField.setBounds(201, 255, 354, 31);
		add(passwordTextField);

		studyYearTextField = new JTextField();
		studyYearTextField.setColumns(10);
		studyYearTextField.setBounds(201, 314, 354, 31);
		add(studyYearTextField);

		lblName = new JLabel("Name");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblName.setBounds(new Rectangle(0, 0, 209, 31));
		lblName.setBounds(34, 32, 209, 31);
		add(lblName);

		lblSurname = new JLabel("Surname");
		lblSurname.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblSurname.setBounds(new Rectangle(0, 0, 209, 31));
		lblSurname.setBounds(34, 77, 209, 31);
		add(lblSurname);

		lblCnp = new JLabel("CNP");
		lblCnp.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblCnp.setBounds(new Rectangle(0, 0, 209, 31));
		lblCnp.setBounds(34, 129, 209, 31);
		add(lblCnp);

		specializationComboBox = new JComboBox<Specialization>();
		specializationComboBox.setBounds(201, 369, 354, 31);
		add(specializationComboBox);

		lblSpecialization = new JLabel("Specialization");
		lblSpecialization.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblSpecialization.setBounds(new Rectangle(0, 0, 209, 31));
		lblSpecialization.setBounds(34, 368, 209, 31);
		add(lblSpecialization);

		rdbtnAdmin = new JRadioButton("Admin");
		rdbtnAdmin.setFont(new Font("Tahoma", Font.PLAIN, 14));
		rdbtnAdmin.setBounds(187, 444, 116, 35);
		add(rdbtnAdmin);

		rdbtnTeacher = new JRadioButton("Teacher");
		rdbtnTeacher.setFont(new Font("Tahoma", Font.PLAIN, 14));
		rdbtnTeacher.setBounds(322, 444, 116, 35);
		add(rdbtnTeacher);

		rdbtnStudent = new JRadioButton("Student");
		rdbtnStudent.setFont(new Font("Tahoma", Font.PLAIN, 14));
		rdbtnStudent.setBounds(455, 444, 116, 35);
		add(rdbtnStudent);

		ButtonGroup buttonGroup = new ButtonGroup();
		buttonGroup.add(rdbtnStudent);
		buttonGroup.add(rdbtnAdmin);
		buttonGroup.add(rdbtnTeacher);

		rdbtnStudent.setSelected(true);

		btnSave = new JButton("Save");
		btnSave.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnSave.setBounds(279, 504, 209, 35);
		add(btnSave);

		lblInternalEmail = new JLabel("Internal email");
		lblInternalEmail.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblInternalEmail.setBounds(new Rectangle(0, 0, 209, 31));
		lblInternalEmail.setBounds(34, 172, 209, 31);
		add(lblInternalEmail);

		lblExternalEmail = new JLabel("External email");
		lblExternalEmail.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblExternalEmail.setBounds(new Rectangle(0, 0, 209, 31));
		lblExternalEmail.setBounds(34, 211, 209, 31);
		add(lblExternalEmail);

		lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPassword.setBounds(new Rectangle(0, 0, 209, 31));
		lblPassword.setBounds(34, 254, 209, 31);
		add(lblPassword);

		lblStudyYear = new JLabel("Study Year");
		lblStudyYear.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblStudyYear.setBounds(new Rectangle(0, 0, 209, 31));
		lblStudyYear.setBounds(34, 313, 209, 31);
		add(lblStudyYear);

		btnSave.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				adminModifyUserController.saveUser();
			}
		});

		rdbtnAdmin.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				adminModifyUserController.selectAdmin();
			}
		});

		rdbtnStudent.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				adminModifyUserController.selectStudent();
			}
		});

		rdbtnTeacher.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				adminModifyUserController.selectTeacher();
			}
		});

		specializationComboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				adminModifyUserController
						.selectSpecialization((Specialization) specializationComboBox.getSelectedItem());
			};
		});

	}

	@Override
	public void setLanguageBundle(ResourceBundle languageBundle) {

		lblName.setText(languageBundle.getString("name"));
		lblSurname.setText(languageBundle.getString("surname"));
		lblCnp.setText(languageBundle.getString("cnp"));
		lblExternalEmail.setText(languageBundle.getString("externalEmail"));
		lblInternalEmail.setText(languageBundle.getString("internalEmail"));
		lblPassword.setText(languageBundle.getString("password"));
		lblStudyYear.setText(languageBundle.getString("year"));
		rdbtnAdmin.setText(languageBundle.getString("admin"));
		rdbtnStudent.setText(languageBundle.getString("student"));
		rdbtnTeacher.setText(languageBundle.getString("teacher"));
		lblSpecialization.setText(languageBundle.getString("specialization"));
		btnSave.setText(languageBundle.getString("save"));

	}

	public String getName() {
		return nameTextField.getText();
	}

	public String getSurname() {
		return surnameTextField.getText();
	}

	public String getCNP() {
		return cnpTextField.getText();
	}

	public String getInternalEmail() {
		return internalEmailTextField.getText();
	}

	public String getExternalEmail() {
		return externalEmailTextField.getText();
	}

	public String getPassword() {
		return passwordTextField.getText();
	}

	public Integer getStudyYear() {
		return new Integer(studyYearTextField.getText());
	}

	public void ereaseAll() {

		specializationComboBox.removeAll();
		nameTextField.setText("");
		surnameTextField.setText("");
		cnpTextField.setText("");
		studyYearTextField.setText("");
		internalEmailTextField.setText("");
		externalEmailTextField.setText("");
		passwordTextField.setText("");

	}

	public void populateSpecializations(List<Specialization> specializations) {
		DefaultComboBoxModel<Specialization> model = (DefaultComboBoxModel) specializationComboBox.getModel();
		model.removeAllElements();
		if (specializations == null || specializations.isEmpty()) {
			return;
		}

		for (Specialization specialization : specializations) {

			specializationComboBox.addItem(specialization);
		}

		adminModifyUserController.selectSpecialization((Specialization) specializationComboBox.getSelectedItem());
	}

	public void setSpecialization(Specialization specialization) {

		for (int i = 0; i < specializationComboBox.getModel().getSize(); i++) {

			if (specializationComboBox.getModel().getElementAt(i).equals(specialization)) {
				specializationComboBox.getModel().setSelectedItem(specialization);
				break;
			}
		}
	}

	public void setName(String surname) {
		// TODO Auto-generated method stub

	}

	public void setSurname(String surname) {
		// TODO Auto-generated method stub

	}

	public void setCnp(String cnp) {
		// TODO Auto-generated method stub

	}

	public void setRole(String role) {
		// TODO Auto-generated method stub

	}
}
