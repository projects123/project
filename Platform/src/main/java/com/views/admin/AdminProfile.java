package com.views.admin;

import javax.swing.JPanel;
import javax.swing.JTextField;

import com.views.common.ILanguagePanel;

import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;
import java.awt.SystemColor;

public class AdminProfile extends JPanel implements ILanguagePanel{
	private JTextField nameTextField;
	private JTextField surnameTextField;
	private JTextField cnpTextField;
	private JTextField externalEmailTextField;
	private JTextField internalEmailTextField;
	private JTextField newPassTextField;
	private JTextField confirmPassTextField;
	private JTextField confirmPasswordTextField;
	
	private JLabel lblName;
	private JLabel lblSurname;
	private JLabel lblCnp;
	private JLabel lblExternalEmail;
	private JLabel lblInternalEmail;
	private JLabel lblOldPassword;
	private JLabel lblNewPassword;
	private JLabel lblConfirmNewPassword;
	
	private JButton btnSave;
	private JButton btnChangePassword;
	
	public AdminProfile() {
		
		initialize();
	}
	private void initialize() {
		setLayout(null);
		setBackground(new Color(215, 228, 242));
		
		nameTextField = new JTextField();
		nameTextField.setBounds(269, 57, 354, 31);
		add(nameTextField);
		nameTextField.setColumns(10);
		
		surnameTextField = new JTextField();
		surnameTextField.setBounds(269, 101, 354, 31);
		surnameTextField.setColumns(10);
		add(surnameTextField);
		
		cnpTextField = new JTextField();
		cnpTextField.setBounds(269, 145, 354, 31);
		cnpTextField.setColumns(10);
		add(cnpTextField);
		
		externalEmailTextField = new JTextField();
		externalEmailTextField.setBounds(269, 189, 354, 31);
		externalEmailTextField.setColumns(10);
		add(externalEmailTextField);
		
		internalEmailTextField = new JTextField();
		internalEmailTextField.setBounds(269, 233, 354, 31);
		internalEmailTextField.setColumns(10);
		add(internalEmailTextField);
		
		
		newPassTextField = new JTextField();
		newPassTextField.setBounds(269, 335, 354, 31);
		newPassTextField.setColumns(10);
		add(newPassTextField);
		
		confirmPassTextField = new JTextField();
		confirmPassTextField.setBounds(269, 389, 354, 31);
		confirmPassTextField.setColumns(10);
		add(confirmPassTextField);
		
		btnSave = new JButton("Save");
		btnSave.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnSave.setForeground(SystemColor.activeCaptionText);
		btnSave.setBounds(207, 513, 209, 35);
		add(btnSave);
		
		btnChangePassword = new JButton("Change password");
		btnChangePassword.setForeground(SystemColor.activeCaptionText);
		btnChangePassword.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnChangePassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnChangePassword.setBounds(489, 513, 209, 35);
		add(btnChangePassword);
		
		lblName = new JLabel("Name");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblName.setBounds(58, 56, 209, 31);
		add(lblName);
		
		lblSurname = new JLabel("Surname");
		lblSurname.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblSurname.setBounds(58, 101, 209, 31);
		add(lblSurname);
		
		lblCnp = new JLabel("CNP");
		lblCnp.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblCnp.setBounds(58, 144, 209, 31);
		add(lblCnp);
		
		lblExternalEmail = new JLabel("External e-mail");
		lblExternalEmail.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblExternalEmail.setBounds(58, 188, 209, 31);
		add(lblExternalEmail);
		
		lblInternalEmail = new JLabel("Internal e-mail");
		lblInternalEmail.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblInternalEmail.setBounds(58, 233, 209, 31);
		add(lblInternalEmail);
		
		lblOldPassword = new JLabel("Old password");
		lblOldPassword.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblOldPassword.setBounds(59, 341, 209, 31);
		add(lblOldPassword);
		
		lblNewPassword = new JLabel("New password");
		lblNewPassword.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewPassword.setBounds(59, 388, 209, 31);
		add(lblNewPassword);
		
		lblConfirmNewPassword = new JLabel("Confirm new password");
		lblConfirmNewPassword.setForeground(SystemColor.activeCaptionText);
		lblConfirmNewPassword.setFont(new Font("Arial", Font.PLAIN, 15));
		lblConfirmNewPassword.setBounds(59, 436, 209, 31);
		add(lblConfirmNewPassword);
		
		confirmPasswordTextField = new JTextField();
		confirmPasswordTextField.setColumns(10);
		confirmPasswordTextField.setBounds(269, 437, 354, 31);
		add(confirmPasswordTextField);
	}  
	
	@Override
	public void setLanguageBundle(ResourceBundle languageBundle)  {
	
      lblName.setText(languageBundle.getString("name"));             
      lblSurname.setText(languageBundle.getString("surname"));         
      lblCnp.setText(languageBundle.getString("cnp"));            
      lblExternalEmail.setText(languageBundle.getString("externalEmail"));
      lblInternalEmail.setText(languageBundle.getString("internalEmail"));
      lblOldPassword.setText(languageBundle.getString("oldPassword"));
      lblNewPassword.setText(languageBundle.getString("newPassword"));
      lblConfirmNewPassword.setText(languageBundle.getString("confirmPassword"));
      btnSave.setText(languageBundle.getString("save"));
      btnChangePassword.setText(languageBundle.getString("changePassword"));
	}	
	
	public void setUsername(String name) {
		nameTextField.setText(name);
	}

	public void setSurname(String surname) {
		surnameTextField.setText(surname);
	}

	public void setCNP(String cnp) {
		cnpTextField.setText(cnp);
	}
	
	public void setExternalEmail(String externalEmail) {
		externalEmailTextField.setText(externalEmail);
	}
	
	
	public void setInternalEmail(String internalEmail) {
		internalEmailTextField.setText(internalEmail);
	}
	
	public void ereaseAll() {
		
		nameTextField.setText("");
		surnameTextField.setText("");	
		cnpTextField.setText("");	
		externalEmailTextField.setText("");
		internalEmailTextField.setText("");             
	}
}
